/**
 * @file Written by Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 *       http://www.absyx.fr
 * 
 * Portions of code: Copyright (c) 2003-2010, CKSource - Frederico Knabben. All
 * rights reserved. For licensing, see LICENSE.html or
 * http://ckeditor.com/license
 */
(function($) {

CKEDITOR.plugins.add('dropcap', {
  init: function(editor) {
    editor.addCommand('insertdropcap', {
      exec : function(editor) {
       var mySelection = editor.getSelection();
       if (CKEDITOR.env.ie) {
         mySelection.unlock(true);
         selectedText = mySelection.getNative().createRange().text;
         }
       else {
         selectedText = mySelection.getNative();
         }
       editor.insertHtml('[dropcap]' + selectedText + '[/dropcap]');
       }
    });
    editor.ui.addButton('dropcap', {
      label: 'Insert dropcap',
      command: 'insertdropcap',
      icon: this.path + 'dropcap.png',
      });
    }
});

})(jQuery);
